#!/usr/bin/env python3

from flask import Flask, request, render_template
from datetime import datetime
import argparse
import time
import os


parser = argparse.ArgumentParser(description='HTTP Echo Server')
parser.add_argument(
	"-t", "--text",
	type=str,
	dest="text",
	default="Hello, World!",
	help="The text to echo; default: \"Hello, World!\"")
parser.add_argument(
	"-p", "--port",
	type=int,
 	dest="port", 
	default=1234,
	help="Port number to bind service to and listen on; default=1234")
parser.add_argument(
	"-d", "--delay",
	type=int,
 	dest="delay", 
	default=0,
	help="Delay in seconds to wait before returning; default=0 (disabled)")
args = parser.parse_args()


app = Flask(__name__)  # Reference __name__ of this file so Flask knows where to look for template files.


@app.route("/", methods=["GET"])
def get_index():
	if args.delay > 0:
		time.sleep(args.delay)
	return args.text + "\n"
#   return flask.render_template("index.html")

@app.route("/hostname", methods=["GET"])
def get_hostname():
	return os.getenv("HOSTNAME") + "\n"

@app.route("/date", methods=["GET"])
def get_date():
	return datetime.utcnow().isoformat() + "\n"


print("Hello, I'll be your Text Server today!")
app.run(debug=False, host='0.0.0.0', port=args.port)

